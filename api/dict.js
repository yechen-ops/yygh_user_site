import request from '../utils/request'

const api_url = '/admin/cmn/dict';

export default {
    // 根据 dictCode 获取下级节点
    findByDictCode(dictCode) {
        return request({
            url: `${api_url}/findByDictCode/${dictCode}`,
            method: 'get'
        })
    },

    // 根据 parentId 获取子数据
    findByParentId(parentId) {
        return request({
            url: `${api_url}/findByParentId/${parentId}`,
            method: 'get'
        })
    }
}