import request from '../utils/request'

const api_url = '/api/hosp/hospital';

export default {
    // 查询医院列表
    getPageList(current, limit, searchObj) {
        return request({
            url: `${api_url}/getHospitalList/${current}/${limit}`,
            method: 'get',
            params: searchObj
        })
    },

    // 根据医院的名称进行模糊查询
    getHospitalByHosname(hosname) {
        return request({
            url: `${api_url}/getHospitalByHosname/${hosname}`,
            method: 'get'
        })
    },

    // 通过医院编号获取科室信息
    getDepartmentByHoscode(hoscode) {
        return request({
            url: `${api_url}/getDepartment/${hoscode}`,
            method: 'get'
        })
    },

    // 获取医院详情
    getHospDetail(hoscode) {
        return request({
            url: `${api_url}/getHospDetail/${hoscode}`,
            method: 'get'
        })
    }

}