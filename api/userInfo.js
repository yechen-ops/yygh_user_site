import request from '../utils/request'

const api_url = `/api/user`

export default {
    // 获取信息，进行登录+注册操作
    login(userInfo) {
        return request({
            url: `${api_url}/loginByPhone`,
            method: `post`,
            data: userInfo
        })
    },

    // 发送邮件验证码
    sendCode(phone) {
        return request({
            url: `/api/email/sendEmail/${phone}`,
            method: `get`
        })
    },

    // 获取用户信息（用户id会从请求头传递到服务器）
    getUserInfo() {
        return request({
            url: `${api_url}/auth/getUserInfo`,
            method: 'get'
        })
    },

    // 添加用户验证信息（传递用户信息 json 格式）
    addUserAuth(userInfo) {
        return request({
            url: `${api_url}/auth/addUserAuth`,
            method: 'post',
            data: userInfo
        })
    }

}
