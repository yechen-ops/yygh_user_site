import request from '@/utils/request'
const api_url = `/api/user/patient`
export default {

    // 添加就诊人信息
    savePatient(patient) {
        return request({
            url: `${api_url}/auth/savePatient`,
            method: 'post',
            data: patient
        })
    },

    // 获取就诊人列表
    findAllPatient() {
        return request({
            url: `${api_url}/auth/findAll`,
            method: `get`
        })
    },

    // 根据就诊人id获取就诊人信息
    getPatientByPatientId(patientId) {
        return request({
            url: `${api_url}/auth/getPatient/${patientId}`,
            method: 'get'
        })
    },
    
    //修改就诊人信息
    updatePatient(patient) {
        return request({
            url: `${api_url}/auth/updatePatient`,
            method: 'put',
            data: patient
        })
    },

    //删除就诊人信息
    removePatient(patientId) {
        return request({
            url: `${api_url}/auth/removePatient/${patientId}`,
            method: 'delete'
        })
    }
}